package quartet.model;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Introspected
public class Card {

    @NotNull(message = "Card id is required")
    private Long id;

    @NotBlank(message = "Card name is required")
    private String name;

    @NotNull(message = "Card theme is required")
    private Theme theme;

    @NotBlank(message = "Card siblings list is required")
    @Size(min = 3, max = 3, message = "Card siblings list must have 3 elements")
    private List<String> siblings;

    public Card() {
    }

    public Card(Long id, String name, Theme theme, List<String> siblings) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.siblings = siblings;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Theme getTheme() {
        return theme;
    }

    public List<String> getSiblings() {
        return siblings;
    }

}
