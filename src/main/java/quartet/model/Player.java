package quartet.model;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Introspected
public class Player {

    private Long id;

    @NotBlank(message = "Player name is required")
    private String name;

    @NotBlank(message = "Player IP-address is required")
    private String ipAddress;

    public Player() {
    }

    public Player(Long id, String name, String ipAddress) {
        this.id = id;
        this.name = name;
        this.ipAddress = ipAddress;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

}
