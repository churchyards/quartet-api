package quartet.model;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;

@Introspected
public class PlayerInfo {

    @NotBlank(message = "Player name is required")
    private String name;

    @NotBlank(message = "Player IP-address is required")
    private String ipAddress;

    @NotBlank(message = "Player tatus is required")
    private Status status;

    public PlayerInfo() {
    }

    public PlayerInfo(final String name, final String ipAddress, final Status status) {
        this.name = name;
        this.ipAddress = ipAddress;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Status getStatus() {
        return status;
    }
}
