package quartet.model;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Introspected
public class Status {

    @PositiveOrZero(message = "Total number of cards should not be less than 0")
    private int totalNumberOfCards;

    @NotBlank(message = "Quartets list is required")
    private List<String> quartets;

    private int numberOfCards;

    public Status() {
    }

    public Status(int totalNumberOfCards, List<String> quartets) {
        this.totalNumberOfCards = totalNumberOfCards;
        this.quartets = quartets;
        numberOfCards = totalNumberOfCards - (quartets.size() * 4);
    }

    public List<String> getQuartets() {
        return quartets;
    }

    public int getTotalNumberOfCards() {
        return totalNumberOfCards;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }
}
