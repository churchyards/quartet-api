package quartet.model;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Introspected
public class Transfer {

    @NotBlank(message = "Theme name is required")
    private String themeName;

    @NotBlank(message = "Card name is required")
    private String cardName;

    @NotBlank(message = "From player name is required")
    private String from;

    @NotBlank(message = "To player name is required")
    private String to;

    private LocalDateTime timestamp = LocalDateTime.now();

    public Transfer() {
    }

    public Transfer(String themeName, String cardName, String from, String to) {
        this.themeName = themeName;
        this.cardName = cardName;
        this.from = from;
        this.to = to;
    }

    public String getThemeName() {
        return themeName;
    }

    public String getCardName() {
        return cardName;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
