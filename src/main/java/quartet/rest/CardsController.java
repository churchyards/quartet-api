package quartet.rest;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.swagger.v3.oas.annotations.Operation;
import quartet.model.Card;
import quartet.service.GameService;

import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/cards")
public class CardsController {

    @Inject
    private GameService gameService;

    /**
     * @param playerId The generated ID of the player. Note that this ID has to be kept secret to other players in order
     *                 not let them call this endpoint and see which cards you have.
     * @return List of cards. Each card object consists of a card name, theme name, theme id and a list of sibling cards that
     * belong to same theme. The theme id is a random number and takes care that players can only ask cards from other players
     * when they own at least one card of that particular theme.
     */
    @Operation(description = "Before the players start the game, they have to retrieve the initial set of cards by calling " +
            "this endpoint. Note that subsequent calls to this endpoint will show the current set of cards of the player at that moment in the game.")
    @Get("/{playerId}")
    public List<Card> getCardsFor(@NotBlank(message = "Player id is required") @PathVariable final Long playerId) {
        return gameService.getCardsFor(playerId);
    }

    /**
     * @param playerId The ID of the player that has received the card, and to who the card needs to be transferred.
     * @param card The card that is transferred
     */
    @Operation(description = "After a player receives a card from an other player, this transfer needs to be posted to the game server!")
    @Post("/{playerId}")
    public void transferCardTo(@NotBlank(message = "Player id is required") @PathVariable final Long playerId,
                               @Body final Card card) {
        gameService.transferCardTo(playerId, card);
    }
}
