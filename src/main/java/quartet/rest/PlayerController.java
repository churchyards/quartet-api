package quartet.rest;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import quartet.model.Player;
import quartet.model.PlayerInfo;
import quartet.service.GameService;

import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/players")
public class PlayerController {

    @Inject
    private GameService gameService;

    /**
     * @param player The player data, i.e. a unique name and an IP-address of the player's device
     * @return The player with a generated ID. Please note this ID! It has to be used for subsequent calls to the other endpoints
     */
    @Operation(description = "Endpoint to register for the game. This can be done for example with IntelliJ REST Client " +
            "tool or Postman. When you want to change the player's name or IP-address, then you have to remove the player and register again.")
    @Post
    public Player register(@Body final Player player) {
        return gameService.addPlayer(player.getName(), player.getIpAddress());
    }

    /**
     * @param playerId The generated ID of the player
     * @return The HttpStatus: OK or player NOT_FOUND
     */
    @Operation(description = "The player can be removed from the game as long as it did not receive a set of cards.")
    @Delete("/{playerId}")
    public HttpStatus removePlayer(@NotBlank(message = "Player id is required") final Long playerId) {
        return gameService.removePlayer(playerId) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
    }

    /**
     * @return List of players and their status
     */
    @Operation(description = "This endpoint returns a list of all players and their current state in the game, i.e. the " +
            "number of quartets collected and the number of cards in the hand.")
    @Get
    public List<PlayerInfo> getAllPlayers() {
        return gameService.getAllPlayers();
    }
}
