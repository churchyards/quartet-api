package quartet.rest;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.swagger.v3.oas.annotations.Operation;
import quartet.model.Transfer;
import quartet.service.GameService;

import javax.inject.Inject;
import java.util.List;

@Controller("/transfers")
public class TransferController {

    @Inject
    private GameService gameService;

    /**
     * @return List of transfers: from, to, card name, and timestamp.
     */
    @Operation(description = "This endpoint lists all the card transfers that have taken place during the game.")
    @Get
    public List<Transfer> getAll() {
        return gameService.getAllTransfers();
    }

}
