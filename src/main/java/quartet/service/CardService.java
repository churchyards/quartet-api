package quartet.service;

import quartet.model.Card;
import quartet.model.Theme;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Singleton
public class CardService {

    private final static int NUMBER_OF_PLAYERS = 11;

    private final static String OLD_LANGUAGES = "Old languages";
    private final static String JVM_LANGUAGES = "JVM languages";
    private final static String SCRIPTING_LANGUAGES = "Scripting languages";
    private final static String PURE_FUNCTIONAL_LANGUAGES = "Pure functional languages";
    private final static String DATA_LANGUAGES = "Data oriented languages";
    private final static String EDUCATIONAL_LANGUAGES = "Educational languages";
    private final static String CLI_LANGUAGES = "Command line interface languages";
    private final static String CONCURRENT_LANGUAGES = "Concurrent languages";
    private final static String GL4_LANGUAGES = "Fourth-generation languages";
    private final static String XML_LANGUAGES = "XML-based languages";

    private final static String ESOTERIC_LANGUAGES = "Esoteric languages";
    private final static String JAVA_UNIT_TEST_LIBRARIES = "Java unit test libraries";
    private final static String JAVA_MOCK_LIBRARIES = "Java mock libraries";
    private final static String JS_UNIT_TEST_LIBRARIES = "JavaScript unit test libraries";
    private final static String E2E_TEST_LIBRARIES = "E2E test libraries";
    private final static String OS = "Operating systems";
    private final static String MAINFRAME_OS = "Mainframe operating systems";
    private final static String FAMOUS_SCIENTISTS = "Famous computer scientists";
    private final static String SQL_COMMANDS = "SQL commands";
    private final static String GAMES= "Popular video games";

    private final static String DEVICES= "Devices";
    private final static String IDES= "Integrated development environments";
    private final static String CI_TOOLS= "Continuous integration tools";
    private final static String CLOUD_PLATFORMS= "Cloud platforms";
    private final static String FAMOUS_FEMALE_SCIENTISTS= "Famous female scientists";
    private final static String JS_FRAMEWORKS= "JavaScript frameworks";
    private final static String ENGINEERING_PRACTICES= "Engineering practices";
    private final static String AGILE_METHODOLOGIES= "Agile methodologies";
    private final static String OWASP_RISKS= "OWASP security risks";
    private final static String WE_COMPONENTS_TOOLS= "Web component tools";

    private List<Card> cards = new ArrayList<>();
    private Map<Integer, List<Card>> cardSets = new HashMap<>();
    private int counter = -1;

    public CardService() {
        createCards();
        Collections.shuffle(cards);
        distributeCards();
    }

    public List<Card> getCards() {
        counter++;
        if (counter == NUMBER_OF_PLAYERS) {
            throw new RuntimeException("No more card sets to distribute. Maximum number of players is " + NUMBER_OF_PLAYERS);
        }
        return cardSets.get(counter);
    }

    private void distributeCards() {
        final int numberOfCards = cards.size();
        final int numberOfCardsPerPlayer = getNumberOfCardsPerPlayer(BigDecimal.valueOf(numberOfCards), BigDecimal.valueOf(NUMBER_OF_PLAYERS));

        for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
            final int fromIndex = i * numberOfCardsPerPlayer;

            if (isLastPlayer(i)) {
                // take the rest
                cardSets.put(i, cards.subList(fromIndex, numberOfCards));
                break;
            }

            final int toIndex = fromIndex + numberOfCardsPerPlayer;
            cardSets.put(i, cards.subList(fromIndex, toIndex));
        }
    }

    private int getNumberOfCardsPerPlayer(BigDecimal numberOfCards, BigDecimal numberOfPlayers) {
        return numberOfCards.divide(numberOfPlayers, RoundingMode.CEILING).intValue();
    }

    private boolean isLastPlayer(int index) {
        return index == NUMBER_OF_PLAYERS - 1;
    }

    private void createCards() {
        final Map<Theme, List<String>> themes = new HashMap<>();
        themes.put(new Theme(generateId(), OLD_LANGUAGES), Arrays.asList(new String[]{"FORTRAN", "LISP", "COBOL", "BASIC"}));
        themes.put(new Theme(generateId(), JVM_LANGUAGES), Arrays.asList(new String[]{"Java", "Scala", "Kotlin", "Groovy"}));
        themes.put(new Theme(generateId(), SCRIPTING_LANGUAGES), Arrays.asList(new String[]{"JavaScript", "Python", "PHP", "Ruby"}));
        themes.put(new Theme(generateId(), PURE_FUNCTIONAL_LANGUAGES), Arrays.asList(new String[]{"Haskell", "PureScript", "Elm", "Clean"}));
        themes.put(new Theme(generateId(), DATA_LANGUAGES), Arrays.asList(new String[]{"SQL", "Cypher", "Clipper", "SPARQL"}));
        themes.put(new Theme(generateId(), EDUCATIONAL_LANGUAGES), Arrays.asList(new String[]{"Scratch", "Pascal", "Snap!", "Elan"}));
        themes.put(new Theme(generateId(), CLI_LANGUAGES), Arrays.asList(new String[]{"sh", "bash", "JCL", "PowerShell"}));
        themes.put(new Theme(generateId(), CONCURRENT_LANGUAGES), Arrays.asList(new String[]{"Go", "Erlang", "Clojure", "Ada"}));
        themes.put(new Theme(generateId(), GL4_LANGUAGES), Arrays.asList(new String[]{"IBM Informix-4GL", "Progress 4GL", "Visual FoxPro", "Uniface"}));
        themes.put(new Theme(generateId(), XML_LANGUAGES), Arrays.asList(new String[]{"XSLT", "XPath", "XAML", "Ant"}));
        themes.put(new Theme(generateId(), ESOTERIC_LANGUAGES), Arrays.asList(new String[]{"Brainfuck", "LOLCODE", "Whitespace", "Malbolge"}));
        themes.put(new Theme(generateId(), JAVA_UNIT_TEST_LIBRARIES), Arrays.asList(new String[]{"JUnit", "TestNG", "Spock Framework", "AssertJ"}));
        themes.put(new Theme(generateId(), JAVA_MOCK_LIBRARIES), Arrays.asList(new String[]{"Mockito", "EasyMock", "JMockit", "PowerMock"}));
        themes.put(new Theme(generateId(), JS_UNIT_TEST_LIBRARIES), Arrays.asList(new String[]{"Jest", "Jasmine", "Enzyme", "Chai"}));
        themes.put(new Theme(generateId(), E2E_TEST_LIBRARIES), Arrays.asList(new String[]{"Protractor", "TestCafe", "Cypress", "Nightwatch"}));
        themes.put(new Theme(generateId(), OS), Arrays.asList(new String[]{"Apple iOS", "Google's Android OS", "Apple macOS", "Linux Operating System"}));
        themes.put(new Theme(generateId(), MAINFRAME_OS), Arrays.asList(new String[]{"Z/OS", "MVS", "VSE", "OS/390"}));
        themes.put(new Theme(generateId(), FAMOUS_SCIENTISTS), Arrays.asList(new String[]{"Tim Berners-Lee", "Brendan Eich", "Alan Kay", "Alan Turing"}));
        themes.put(new Theme(generateId(), SQL_COMMANDS), Arrays.asList(new String[]{"INSERT", "UPDATE", "DELETE", "SELECT"}));
        themes.put(new Theme(generateId(), GAMES), Arrays.asList(new String[]{"Call of Duty", "League of Legends", "Fortnite", "Minecraft"}));
        themes.put(new Theme(generateId(), DEVICES), Arrays.asList(new String[]{"mobile phone", "laptop", "tablet", "smartwatch"}));
        themes.put(new Theme(generateId(), IDES), Arrays.asList(new String[]{"IntelliJ IDEA", "Visual Studio Code", "Eclipse", "Xcode"}));
        themes.put(new Theme(generateId(), CI_TOOLS), Arrays.asList(new String[]{"GitLab CI", "Jenkins", "TeamCity", "Travis"}));
        themes.put(new Theme(generateId(), CLOUD_PLATFORMS), Arrays.asList(new String[]{"Amazon Web Services", "Microsoct Azure", "Google Cloud Platform", "Alibaba Cloud"}));
        themes.put(new Theme(generateId(), FAMOUS_FEMALE_SCIENTISTS), Arrays.asList(new String[]{"Ada Lovelace", "Grace Hopper", "Barbara Liskov", "Adele Goldberg"}));
        themes.put(new Theme(generateId(), JS_FRAMEWORKS), Arrays.asList(new String[]{"Angular", "React", "Vue", "Ember"}));
        themes.put(new Theme(generateId(), ENGINEERING_PRACTICES), Arrays.asList(new String[]{"Behavior Driven Development", "Test Driven Development", "Domain Driven Development", "Component Driven Development"}));
        themes.put(new Theme(generateId(), AGILE_METHODOLOGIES), Arrays.asList(new String[]{"Scrum", "Lean", "eXtreme Programming", "Kanban"}));
        themes.put(new Theme(generateId(), OWASP_RISKS), Arrays.asList(new String[]{"Injection", "Cross-Site Scripting", "Sensitive Data Exposure", "Broken Authentication"}));
        themes.put(new Theme(generateId(), WE_COMPONENTS_TOOLS), Arrays.asList(new String[]{"Svelte", "Polymer", "Lit", "Stencil"}));

        themes.forEach( (theme, names) -> names.stream()
                .forEach( name -> cards.add(new Card(generateId(), name, theme, names.stream()
                        .filter( n -> !n.equalsIgnoreCase(name) )
                        .collect(Collectors.toList() ) ) ) ) );
    }

    private Long generateId() {
        Random random = new Random();
        return Long.valueOf( random.nextInt(9000) + 1000 );
    }
}
