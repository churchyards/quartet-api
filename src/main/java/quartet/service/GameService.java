package quartet.service;

import quartet.model.Card;
import quartet.model.Player;
import quartet.model.PlayerInfo;
import quartet.model.Status;
import quartet.model.Theme;
import quartet.model.Transfer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Singleton
public class GameService {

    private final List<Player> players = new ArrayList<>();
    private final Map<String, List<Card>> playerCards = new HashMap<>();
    private final List<Transfer> transfers = new ArrayList<>();

    @Inject
    private CardService cardService;

    public Player addPlayer(final String name, final String ipAddress) {
        if (isRegistered(name)) {
            throw new RuntimeException(String.format("Player '%s' is already registered.", name));
        }
        final Player player = new Player(generateId(), name, ipAddress);
        players.add(player);
        return player;
    }

    public boolean removePlayer(final Long playerId) {
        final Player player = findPlayerById(playerId);
        if (hasReceivedCards(player)) {
            throw new RuntimeException(String.format("Player '%s' already has received cards and can not be removed from the game anymore.", player.getName()));
        }
        return players.removeIf( p -> p.getName().equalsIgnoreCase(player.getName()) );
    }

    public List<PlayerInfo> getAllPlayers() {
        return players.stream()
                .map( p -> new PlayerInfo(p.getName(), p.getIpAddress(), new Status(getTotalNumberOfCardsFor(p), getQuartetsFor(p))) )
                .collect(Collectors.toList());
    }

    // to receive initial or current set of cards
    public List<Card> getCardsFor(final Long playerId) {
        final Player player = findPlayerById(playerId);
        if (hasReceivedCards(player)) {
            return playerCards.get(player.getName());
        }
        final List<Card> cards = cardService.getCards();
        playerCards.put(player.getName(), cards);
        return cards;
    }

    public synchronized  void transferCardTo(final Long playerId, final Card card) {
        final Player toPlayer = findPlayerById(playerId);
        final String fromPlayerName = findPlayerNameFor(card);

        // can not transfer to yourself
        if (fromPlayerName.equalsIgnoreCase(toPlayer.getName())) {
            throw new RuntimeException(String.format("Can not tranfer card '%s' > '%s' to yourself '%s'", card.getTheme().getName(), card.getName(), fromPlayerName));
        }

        // remove card from player
        playerCards.entrySet().stream()
                .forEach( entry -> entry.setValue(
                        entry.getValue().stream()
                                .filter( c -> !c.getId().equals(card.getId()) )
                                .collect(Collectors.toList())
                        )
                );
        // add card to player
        final List<Card> cards = playerCards.get(toPlayer.getName());
        cards.add(card);

        // register transfer
        transfers.add(new Transfer(card.getTheme().getName(), card.getName(), fromPlayerName, toPlayer.getName()));
    }

    public List<Transfer> getAllTransfers() {
        return transfers;
    }

    private Long generateId() {
        Random random = new Random();
        return Long.valueOf( random.nextInt(9000) + 1000 );
    }

    private Player findPlayerById(final Long playerId) {
        return players.stream()
                .filter( p -> p.getId().equals(playerId) )
                .findFirst()
                .orElseThrow( () -> new RuntimeException(String.format("Player with id '%d' is not registered. Please register first.", playerId)) );
    }

    private boolean isRegistered(final String playerName) {
        return players.stream().anyMatch( p -> p.getName().equalsIgnoreCase(playerName) );
    }

    private boolean hasReceivedCards(final Player player) {
        return playerCards.get(player.getName()) != null;
    }

    private int getTotalNumberOfCardsFor(final Player player) {
        if (playerCards.get(player.getName()) == null) {
            return 0;
        }
        return playerCards.get(player.getName()).size();
    }

    private List<String> getQuartetsFor(final Player player) {
        if (playerCards.get(player.getName()) == null) {
            return new ArrayList<>();
        }
        final List<Card> cards = playerCards.get(player.getName());
        final Map<String, Long> counter = cards.stream()
                .collect(Collectors.groupingBy( c -> c.getTheme().getName(), Collectors.counting()));
        return counter.entrySet().stream()
                .filter( entry -> entry.getValue().equals(4L) )
                .map( entry -> entry.getKey() )
                .collect(Collectors.toList());
    }

    private String findPlayerNameFor(final Card card) {
        return playerCards.entrySet().stream()
                .filter( entry -> containsCard(card, entry.getValue()) )
                .findFirst()
                .orElseThrow( () -> new RuntimeException(String.format("Can not find player that owns card '%s' > '%s'", card.getTheme().getName(), card.getName())) )
                .getKey();
    }

    private boolean containsCard(final Card card, final List<Card> cards) {
        return cards.stream().anyMatch( c -> c.getId().equals(card.getId()) );
    }
}
